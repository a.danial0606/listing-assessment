//
//  LoginPage.swift
//  Assessment_Normal
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 08/11/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper

class LoginPage: UIViewController {

    @IBOutlet weak var loginPageView: UIView!
    @IBOutlet weak var loginViewY: NSLayoutConstraint!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var rememberMeImg: UIImageView!
    @IBOutlet weak var rememberMeLbl: UILabel!
    @IBOutlet weak var loginTitleLbl: UILabel!
    
    var popUp: IQPopUp?
    var popUpColor:UIColor?
    var strMsg: String!
    var isRememberMe: Bool = false
    weak var textfield: UITextField!
    fileprivate var ColorPopUpBg = UIColor(red: 0.702, green: 0.000, blue: 0.000, alpha: 1.000)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.setupView()
        
        self.loginBtn.addTarget(self, action: #selector(self.doLogin(_:)), for: .touchUpInside)
        self.usernameTextField.delegate = self
        self.usernameTextField.tag = 0
        self.passwordTextField.delegate = self
        self.passwordTextField.tag = 1
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(doRememberMe))
        self.rememberMeImg.addGestureRecognizer(tapGesture)
        self.rememberMeImg.isUserInteractionEnabled = true
        
        self.rememberMeImg.image = self.isRememberMe ? UIImage(named: "filter-checked.png") : UIImage(named: "filter-uncheck.png")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.removeSpinner()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func doRememberMe(){
        self.isRememberMe = !self.isRememberMe
        
        if self.isRememberMe{
            self.rememberMeImg.image = UIImage(named: "filter-checked.png")
        }else{
            self.rememberMeImg.image = UIImage(named: "filter-uncheck.png")
        }
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
          if let userInfo = notification.userInfo {
              let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
              let endFrameY = endFrame?.origin.y ?? 0
              let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
              let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
              let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
              let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
              
              if endFrameY >= UIScreen.main.bounds.size.height {
                self.loginViewY.constant = -((endFrameY - (endFrame?.size.height ?? 0.0))/2)
              } else {
                self.loginViewY.constant = -((endFrameY - (endFrame?.size.height ?? 0.0))/2)
              }
              
              UIView.animate(withDuration: duration,
                             delay: TimeInterval(0),
                                         options: animationCurve,
                                         animations: { self.view.layoutIfNeeded() },
                                         completion: nil)
          }
      }
    
    @objc func keyboardWillDisappear(notification: NSNotification) {
        //Do something here
         if let userInfo = notification.userInfo {
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
                   
            self.loginViewY.constant = 0.0
                   
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: { self.view.layoutIfNeeded() }, completion: nil)
        }
    }
    
    @objc func doLogin(_ sender: UIButton){
        
        self.removeErrorIcon()
        
        if self.usernameTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
             self.errorMessageShow(textfield: self.usernameTextField, text: "Please enter email.")
        }else if !self.isValidEmail(email: self.usernameTextField.text!){
            self.errorMessageShow(textfield: self.usernameTextField, text: "Invalid email address.")
        }else if self.passwordTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            self.errorMessageShow(textfield: self.passwordTextField, text: "Please enter password.")
        }else{
            self.showSpinner()
            let urlPath = "https://interview.advisoryapps.com/index.php/login"
            
            let parameters: [String: Any] =
            ["email"        : self.usernameTextField.text!,
            "password"      : self.passwordTextField.text!]
            
            Alamofire.request(urlPath, method: .post, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON {
                       response in
                       
                       switch (response.result) {
                       case .success:
                            if let responseData: NSDictionary = response.result.value as? NSDictionary {
                                 let json = JSON(responseData)
                                 
                                if json["status"]["code"] == 200{                                    
                                    KeychainWrapper.standard.set(crypto.encryptData(dataToEncrypt: json["token"].stringValue), forKey: "token")
                                    UserDefaults.standard.set(json["id"].stringValue, forKey: "userID")
                                    
                                    if self.isRememberMe{
                                        //another way is to store credentials using network credentials to the keychain provided by apple
                                        KeychainWrapper.standard.set(crypto.encryptData(dataToEncrypt: self.usernameTextField.text!), forKey: "username")
                                        KeychainWrapper.standard.set(crypto.encryptData(dataToEncrypt: self.passwordTextField.text!), forKey: "password")
                                    }
                                    self.goToListingPage()
                                }else{
                                    self.removeSpinner()
                                    if json["status"]["message"].isEmpty{
                                        self.alertSingleBtn(title: "Alert", message: "Unable to login. Please try again", btnTitle: "OK") { (_) in
                                           //do nothing
                                        }
                                    }else{
                                        self.alertSingleBtn(title: "Alert", message: json["status"]["message"].stringValue, btnTitle: "OK") { (_) in
                                            //do nothing
                                        }
                                    }
                                }
                            }
                           
                       case .failure(let error):
                           self.removeSpinner()
                           self.alertSingleBtn(title: "Alert", message: "Unable to login. Please try again", btnTitle: "OK") { (_) in
                              //do nothing
                           }
                           #if DEBUG
                           print("code error: \(error.localizedDescription)")
                           #endif
                }
            }
        }
    }
    
    @objc func showErrorWithMsg() {
        popUp = IQPopUp(frame: CGRect.zero)
        popUp!.strMsg = self.strMsg as NSString
        popUp!.popUpColor = popUpColor
        popUp!.showOnRect = self.textfield.convert((self.textfield.rightView?.frame)!, to: nil)
        popUp!.fieldFrame = self.textfield.superview!.convert(self.textfield.frame, to: nil)
        popUp!.backgroundColor = UIColor.clear
        self.view.addSubview(popUp!)
        popUp!.translatesAutoresizingMaskIntoConstraints = false
        let dict = ["v1":popUp!]
        popUp?.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[v1]-0-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: dict))
        popUp?.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v1]-0-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: dict))
    }
    
    func isValidEmail(email: String) -> Bool{
        let emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
                            
        if emailTest.evaluate(with: email) == true {
            return true
        } else {
            return false
        }
    }
    
    func showErrorIconForMsg() {
        let containerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width:27, height: self.textfield.frame.size.height))
        let btnError = UIButton(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        btnError.addTarget(self, action: #selector(self.tapOnError), for: .touchUpInside)
        btnError.setBackgroundImage(UIImage(named: "error")!, for: UIControl.State())
        containerView.addSubview(btnError)
        btnError.center = containerView.center
        self.textfield.rightView = containerView
        self.textfield.rightViewMode = .always
    }
    
    func showError(){
        if self.textfield.rightView == nil {
            self.showErrorIconForMsg()
            _ = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.showErrorWithMsg), userInfo: nil, repeats: false)
        }else {
            self.showErrorWithMsg()
        }
    }
    
    func errorMessageShow(textfield: UITextField, text: String){
        self.strMsg = text
        self.textfield = textfield
        self.showError()
    }
    
    @objc func tapOnError() {
        self.showErrorWithMsg()
    }
    
    func removeErrorIcon() {
        self.usernameTextField.rightView = nil
        self.passwordTextField.rightView = nil
    }
    
    func setupView(){
        self.loginTitleLbl.text = "Login"
        self.loginTitleLbl.font = UIFont(name: "DriftWood", size: 50)
        self.rememberMeLbl.text = "Remember Me"
        
        if Application.getUsername().isEmpty && Application.getPassword().isEmpty{
            self.usernameTextField.placeholder = "Username"
            self.passwordTextField.placeholder = "Password"
            self.isRememberMe = false
        }else{
            self.usernameTextField.text = Application.getUsername()
            self.passwordTextField.text = Application.getPassword()
            self.isRememberMe = true
        }
        
        self.passwordTextField.isSecureTextEntry = true
        self.loginBtn.setTitle("Login".uppercased(), for: .normal)
        self.loginBtn.layer.cornerRadius = 6
        
        self.loginPageView.layer.shadowColor = UIColor(red:  0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        self.loginPageView.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.loginPageView.layer.shadowRadius = 1.5
        self.loginPageView.layer.shadowOpacity = 0.2
        self.loginPageView.layer.cornerRadius = 6
        
        self.popUpColor = self.ColorPopUpBg
    }
    
    func goToListingPage(){
         Application.setRootPage(identifier: "ListingPageNav")
    }


}

extension LoginPage: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.rightView = nil
    }
     
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
          nextField.becomeFirstResponder()
       } else {
          textField.resignFirstResponder()
       }
       return false
    }
}
