//
//  ListingCollectionReusableView.swift
//  Assessment_Normal
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 10/11/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import UIKit

class ListingCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var topViewLbl: UILabel!
    @IBOutlet weak var listSegment: UISegmentedControl!
    
}
