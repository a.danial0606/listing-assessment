//
//  ViewController.swift
//  Assessment_Normal
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 08/11/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper

class ViewController: UIViewController {
    @IBOutlet weak var listCollectionView: UICollectionView!
    @IBOutlet weak var listTableView: UITableView!
    
    fileprivate var page: Int = 0
    var arrListingData = [listingData]()
    var subTitleView = UIView()
    var header = UIView()
    var segmentView = UIView()
    var listSegment = UISegmentedControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setupView()
        
        self.setupTableView()
        
        self.listCollectionView.dataSource = self
        self.listCollectionView.delegate = self
        self.listCollectionView.alwaysBounceVertical = true
        self.listCollectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        // Set custom indicator margin
        self.listCollectionView.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset to load data before reaching end of data
        self.listCollectionView.infiniteScrollTriggerOffset = 1000
        
        self.setInfiniteScroll()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.showSpinner()
        self.loadData() { (jsonData) in
             for i in 0..<jsonData["listing"].count {
                 let item = listingData(id: jsonData["listing"][i]["id"].stringValue, list_name: jsonData["listing"][i]["list_name"].stringValue, distance: jsonData["listing"][i]["distance"].stringValue)
                 self.arrListingData.append(item)
             }
             UIView.transition(with: self.listTableView, duration: 0.1, options: .transitionCrossDissolve, animations: {
                 self.listTableView.reloadData()
             }) { (true) in
                 //remove loading when everything is loaded
                 self.removeSpinner()
             }
        }
    }
    
    func setupView(){
        self.title = "Listing"
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(doLogout))
        
        self.listCollectionView.isHidden = true
        self.listTableView.isHidden = false
    }
    
    func setupTableView(){
        let valueToMinus = self.getDeviceScreen(condition: "tableView")
        let string = "Hello testing testing testing testing Hello testing testing testing testing Hello testing testing testing testing asd asr a"
        let height = heightForView(text: string, font: UIFont.systemFont(ofSize: 17), width:self.listTableView.frame.size.width - valueToMinus)
        
        self.listTableView.estimatedSectionHeaderHeight = CGFloat(20)
        self.listTableView.contentInsetAdjustmentBehavior = .never
        // Set a header for the table view
        header = UIView(frame: CGRect(x: 0, y: 0, width: self.listTableView.frame.width, height:  height + 10))
        if #available(iOS 13.0, *) {
            header.backgroundColor = .systemBackground
        } else {
            // Fallback on earlier versions
            header.backgroundColor = .white
        }
        
        self.listTableView.tableHeaderView = header
        
        self.listTableView.allowsSelection = false
        self.listTableView.dataSource = self
        self.listTableView.delegate = self
        self.listTableView.tableFooterView = UIView()
        
        // Set custom indicator margin
        self.listTableView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        self.listTableView.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset to load data before reaching end of data
        self.listTableView.infiniteScrollTriggerOffset = 1000
        
        let subTitle = UILabel(frame: CGRect(x: 0, y: 0, width: self.listTableView.frame.size.width - valueToMinus, height: height))
        subTitle.text = string
        subTitle.textColor = UIColor(named: "DarkNightMode")
        subTitle.textAlignment = .center
        subTitle.numberOfLines = 0
        subTitle.lineBreakMode = .byWordWrapping
        subTitle.font = UIFont.systemFont(ofSize: 17)
        subTitle.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        
        header.addSubview(subTitle)
        subTitle.center = header.center
        
        segmentView = UIView(frame: CGRect(x: 0, y: 0, width: self.listTableView.frame.width, height:  50))
        if #available(iOS 13.0, *) {
            segmentView.backgroundColor = .systemBackground
        } else {
            // Fallback on earlier versions
            segmentView.backgroundColor = .white
        }
        
        let items = ["List 1", "List 2"]
        let segment = UISegmentedControl(items: items)
        segment.selectedSegmentIndex = 0

        // Set up Frame and SegmentedControl
        segment.frame = CGRect(x: subTitle.frame.maxX - self.getDeviceScreen(condition: "tableView1"), y: 20, width: self.getDeviceScreen(condition: "segmentWidth"), height: self.getDeviceScreen(condition: "segmentHeight"))

        // Style the Segmented Control
        segment.layer.cornerRadius = 5.0  // Don't let background bleed
        if #available(iOS 13.0, *) {
            segment.backgroundColor = .systemBackground
        } else {
            // Fallback on earlier versions
            segment.backgroundColor = .white
        }
        segment.tintColor = .systemBlue

        // Add target action method
        segment.addTarget(self, action: #selector(self.selectSegment(_:)), for: .valueChanged)
        
        self.segmentView.addSubview(segment)

    }
    
    func setInfiniteScroll() {
        // Add infinite scroll handler
        self.listTableView.addInfiniteScroll { [weak self] (listTableView) -> Void in
            // load for more data
            self?.page += 1
            
            self?.loadData() { (jsonData) in
               for i in 0..<jsonData["listing"].count {
                       //filter data to avoid duplicate
                       let results = self?.arrListingData.filter { $0.id.elementsEqual(jsonData["listing"][i]["id"].stringValue) }
                       
                       if results?.isEmpty == true {
                           let item = listingData(id: jsonData["listing"][i]["id"].stringValue, list_name: jsonData["listing"][i]["list_name"].stringValue, distance: jsonData["listing"][i]["distance"].stringValue)
                           
                           // tell the table view to update data at indexpath
                           self?.reloadData(item: item)
                       }
                   }
                   listTableView.finishInfiniteScroll()
            }
        }
       
        self.listCollectionView.addInfiniteScroll { [weak self] (listCollectionView) -> Void in
            // load for more data
            self?.page += 1
                   
            self?.loadData() { (jsonData) in
               for i in 0..<jsonData["listing"].count {
                    //filter data to avoid duplicate
                    let results = self?.arrListingData.filter { $0.id.elementsEqual(jsonData["listing"][i]["id"].stringValue) }
                        
                    if results?.isEmpty == true {
                        let item = listingData(id: jsonData["listing"][i]["id"].stringValue, list_name: jsonData["listing"][i]["list_name"].stringValue, distance: jsonData["listing"][i]["distance"].stringValue)
                        
                        // tell the collection view to update data at indexpath
                        self?.reloadData(item: item)
                    }
                }
                listCollectionView.finishInfiniteScroll()
           }
        }
    }
    
    func loadData(completion: @escaping (JSON)->Void) {
        
        let urlPath = "https://interview.advisoryapps.com/index.php/listing?id=\(Application.getUserID())&token=\(Application.getUserToken())&page=\(page)"
      
        Alamofire.request(urlPath, method: .post, encoding: URLEncoding.default).validate().responseJSON {
            response in
            switch (response.result) {
                case .success:
                    if let responseData: NSDictionary = response.result.value as? NSDictionary {
                        let json = JSON(responseData)
                        if json["status"]["code"] == 400{
                            if json["status"]["message"] == "Invalid Token" {
                                //if token not valid/expired, force user relogin
                                self.alertSingleBtn(title: "Alert", message: "Invalid Token. Please relogin", btnTitle: "OK") { (_) in
                                    self.logout()
                                }
                            }
                        }else{
                            completion(json)
                        }
                    }
                case .failure(let error):
                    #if DEBUG
                    print("code error: \(error.localizedDescription)")
                    #endif
            }
                           
        }
    }
    
    func reloadData(item: listingData){
        // Update UI on main thread
        DispatchQueue.main.async(execute: {
            if self.listCollectionView.isHidden == true {
                let indexPath = IndexPath(row: (self.arrListingData.count), section: 0)
                self.listTableView.beginUpdates()
                self.arrListingData.append(item)
                self.listTableView.insertRows(at: [indexPath], with: .automatic)
                self.listTableView.endUpdates()
            }else{
                self.listCollectionView.performBatchUpdates({
                    let indexPath = IndexPath(row: (self.arrListingData.count), section: 0)
                    self.arrListingData.append(item) //add your object to data source first
                    self.listCollectionView.insertItems(at: [indexPath])
                }, completion: nil)
            }
        })
    }
    
    //to get UILabel height
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
    //for hiding header when scrolling the tableview
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.listTableView.isHidden == false {
            if scrollView.contentOffset.y > header.frame.size.height {
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 10, initialSpringVelocity: 0, options: .transitionCrossDissolve, animations:  {
                    self.subTitleView.isHidden = true
                }, completion: {finished in})
            } else {
                if self.subTitleView.isHidden == true { //to avoid from run multiple time become jumpy UI on Iphone SE and below
                    UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 10, initialSpringVelocity: 0, options: .transitionCrossDissolve, animations:  {
                        self.listTableView.beginUpdates()
                        self.subTitleView.isHidden = false
                        self.listTableView.endUpdates()
                    }, completion: {finished in})
                }
            }
        }
    }
    
    func logout(){
        //clear everything and set root page
        KeychainWrapper.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "userID")
        Application.setRootPage(identifier: "LoginPageNav")
    }
    
    //get device screen for UI purposes
    func getDeviceScreen(condition: String) -> CGFloat{
        var returnValue: CGFloat = 0
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
                case 1136:
                // iPhone SE/5/5s
                  returnValue = condition == "collectionView" ? 3 : (condition == "tableView" ? 100 : (condition == "tableView1" ? 151 : (condition == "segmentWidth" ? 97 : 29)))
                case 1920, 1792, 2208, 2688:
                // iPhone 6+/7+/6S+/iphone 8+/iPhone Xr/iphone 11/iphone XS MAX/11 pro max
                  returnValue = condition == "collectionView" ? 4 : (condition == "tableView" ? 20 : (condition == "tableView1" ? 105 : (condition == "segmentWidth" ? 105 : 32)))
                case 2436:
                //iphone 11 pro, XS
                  returnValue = condition == "collectionView" ? 4 : (condition == "tableView" ? 40 : (condition == "tableView1" ? 134 : (condition == "segmentWidth" ? 105 : 32)))
                default:
                //iphone 6, iphone 7, iphone 8
                  returnValue = condition == "collectionView" ? 3 : (condition == "tableView" ? 40 : (condition == "tableView1" ? 134 : (condition == "segmentWidth" ? 105 : 32)))
            }
        }
        return returnValue
    }

    @objc func doLogout(){
        self.alertDoubleBtn(title: "Alert", message: "Are you sure want to logout?", firstBtnTitle: "Yes", secondBtnTitle: "No") { (true) in
            self.logout()
        }
    }
    
    @objc func selectSegment(_ sender: UISegmentedControl){
        
          switch sender.selectedSegmentIndex
          {
              case 1:
                  DispatchQueue.main.async {
                      self.listCollectionView.isHidden = false
                      self.listTableView.isHidden = true
                      sender.selectedSegmentIndex = 0 //reset tableview segmentation
                      self.listCollectionView.reloadData()
                  }
                  break
               default:
                  DispatchQueue.main.async {
                     self.listTableView.isHidden = false
                     self.listCollectionView.isHidden = true
                     self.listTableView.reloadData()
                  }
                  break
              }
      }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListingData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableViewCell", for: indexPath) as! listTableViewCell
        
        cell.listNameLbl.text = self.arrListingData[indexPath.row].list_name
        cell.distanceLbl.text = self.arrListingData[indexPath.row].distance
        
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        self.subTitleView = UIView(frame: CGRect(x: 0, y: 0, width: self.segmentView.frame.width, height:  self.segmentView.frame.height))
        if #available(iOS 13.0, *) {
            self.subTitleView.backgroundColor = .systemBackground
        } else {
            // Fallback on earlier versions
            self.subTitleView.backgroundColor = .white
        }
        self.subTitleView.addSubview(self.segmentView)
        
        return self.subTitleView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerSize = self.subTitleView.isHidden == false ? self.segmentView.frame.height : 0
        return headerSize
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrListingData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCollectionViewCell", for: indexPath) as! listCollectionViewCell
        
        cell.listNameLbl.text = self.arrListingData[indexPath.row].list_name
        cell.distanceLbl.text = self.arrListingData[indexPath.row].distance
        cell.layer.cornerRadius = 10.0
        cell.layer.borderColor = UIColor(named: "DarkNightMode")?.cgColor
        cell.layer.borderWidth = 0.5
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCell", for: indexPath) as! ListingCollectionReusableView
            
            cell.topViewLbl.text = "Hello testing testing testing testing Hello testing testing testing testing Hello testing testing testing testing asd asr a"
            cell.listSegment.addTarget(self, action: #selector(self.selectSegment(_:)), for: .valueChanged)
            cell.listSegment.selectedSegmentIndex = 1
            
            return cell
        default:
             preconditionFailure("Invalid supplementary view type for this collection view")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let yourWidth = collectionView.bounds.width / self.getDeviceScreen(condition: "collectionView")
        let yourHeight = yourWidth

        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

class listTableViewCell: UITableViewCell{
    
    @IBOutlet weak var listNameLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
}

class listCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var listNameLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    
}
