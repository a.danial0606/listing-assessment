//
//  DataModel.swift
//  Assessment_Normal
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 09/11/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import Foundation

struct listingData
{
    let id: String
    let list_name: String
    let distance: String
}
