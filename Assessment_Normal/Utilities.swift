//
//  Utilities.swift
//  Assessment_Normal
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 08/11/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import Foundation
import UIKit
import RNCryptor
import SwiftKeychainWrapper
import NVActivityIndicatorView

class IQPopUp : UIView {
    
    var showOnRect:CGRect?
    var popWidth:Int = 0
    var fieldFrame:CGRect?
    var strMsg:NSString = ""
    var popUpColor:UIColor?
    var FontSize:CGFloat = 15
    
    var PaddingInErrorPopUp:CGFloat = 5
    var FontName = "Helvetica-Bold"
    
    override func draw(_ rect:CGRect) {
        let color = popUpColor!.cgColor.components
        
        UIGraphicsBeginImageContext(CGSize(width: 30, height: 20))
        let ctx = UIGraphicsGetCurrentContext()
        ctx?.setFillColor(red: (color?[0])!, green: (color?[1])!, blue: (color?[2])!, alpha: 1)
        ctx?.setShadow(offset: CGSize(width: 0, height: 0), blur: 7.0, color: UIColor.black.cgColor)
        let points = [ CGPoint(x: 15, y: 5), CGPoint(x: 25, y: 25), CGPoint(x: 5,y: 25)]
        //        Convert code swift 2 to 3 (Kai Nan)
        //        CGContextAddLines(ctx, points, 3)
        ctx?.addLines(between: points)
        ctx?.closePath()
        ctx?.fillPath()
        let viewImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imgframe = CGRect(x: (showOnRect!.origin.x + ((showOnRect!.size.width-30)/2)),
                              y: ((showOnRect!.size.height/2) + showOnRect!.origin.y), width: 30, height: 13)
        
        let img = UIImageView(image: viewImage, highlightedImage: nil)
        
        self.addSubview(img)
        img.translatesAutoresizingMaskIntoConstraints = false
        var dict:Dictionary<String, AnyObject> = ["img":img]
        
        
        img.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[img(%f)]", imgframe.origin.x, imgframe.size.width), options:NSLayoutConstraint.FormatOptions(), metrics:nil, views:dict))
        img.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:|-%f-[img(%f)]",imgframe.origin.y,imgframe.size.height), options:NSLayoutConstraint.FormatOptions(),  metrics:nil, views:dict))
        
        let font = UIFont(name: FontName, size: FontSize)
        
        var size:CGSize = self.strMsg.boundingRect(with: CGSize(width: fieldFrame!.size.width - (PaddingInErrorPopUp*2), height: 1000), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font:font!], context: nil).size
        
        
        size = CGSize(width: ceil(size.width), height: ceil(size.height))
        
        
        let view = UIView(frame: CGRect.zero)
        self.insertSubview(view, belowSubview:img)
        view.backgroundColor=self.popUpColor
        view.layer.cornerRadius=5.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius=5.0
        view.layer.shadowOpacity=1.0
        view.layer.shadowOffset=CGSize(width: 0, height: 0)
        view.translatesAutoresizingMaskIntoConstraints = false
        dict = ["view":view]
        
        view.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[view(%f)]",fieldFrame!.origin.x+(fieldFrame!.size.width-(size.width + (PaddingInErrorPopUp*2))),size.width+(PaddingInErrorPopUp*2)), options:NSLayoutConstraint.FormatOptions(), metrics:nil, views:dict))
        
        view.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:|-%f-[view(%f)]",imgframe.origin.y+imgframe.size.height,size.height+(PaddingInErrorPopUp*2)), options:NSLayoutConstraint.FormatOptions(),  metrics:nil, views:dict))
        
        let lbl = UILabel(frame: CGRect.zero)
        lbl.font = font
        lbl.numberOfLines=0
        lbl.backgroundColor = UIColor.clear
        lbl.text=self.strMsg as String
        lbl.textColor = UIColor.white
        view.addSubview(lbl)
        
        lbl.translatesAutoresizingMaskIntoConstraints = false
        dict = ["lbl":lbl]
        lbl.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[lbl(%f)]", PaddingInErrorPopUp, size.width), options:NSLayoutConstraint.FormatOptions() , metrics:nil, views:dict))
        lbl.superview?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:|-%f-[lbl(%f)]", PaddingInErrorPopUp,size.height), options:NSLayoutConstraint.FormatOptions(), metrics:nil, views:dict))
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        self.removeFromSuperview()
        return false
    }
}


class crypto{
    //this is basic usage. in normal usage can use KDF for generate salt
    private static let encryptionKey = "Adksafdasfa12314@#$"
    
    class func encryptData(dataToEncrypt:String) -> String {
        let messageData = dataToEncrypt.data(using: .utf8)!
        let cipherData = RNCryptor.encrypt(data: messageData, withPassword: encryptionKey)
        return cipherData.base64EncodedString()
    }
    
    class func decryptData(dataToDecrypt:String) -> String {
        guard let encryptedData = Data.init(base64Encoded: dataToDecrypt), let decryptedData = try? RNCryptor.decrypt(data: encryptedData, withPassword: encryptionKey) else{
            #if DEBUG
            print("Wrong data")
            #endif
            return ""
        }
        
        let decryptedString = String(data: decryptedData, encoding: .utf8)!

        return decryptedString
    }
}

class Application: NSObject {
    class func getUserToken() -> String {
        if let token = KeychainWrapper.standard.string(forKey: "token"){
            return crypto.decryptData(dataToDecrypt: token)
        }
        return ""
    }
    
    class func getUsername() -> String {
           if let username = KeychainWrapper.standard.string(forKey: "username"){
               return crypto.decryptData(dataToDecrypt: username)
           }
           return ""
       }
       
    class func getPassword() -> String {
              if let password = KeychainWrapper.standard.string(forKey: "password"){
                  return crypto.decryptData(dataToDecrypt: password)
              }
              return ""
          }
    
    class func getUserID() -> String {
        if let userID = UserDefaults.standard.string(forKey: "userID"){
            return userID
        }
        return ""
    }
    
    class func setRootPage(identifier: String){
       DispatchQueue.main.async {
           let story = UIStoryboard(name: "Main", bundle:nil)
           let vc = story.instantiateViewController(withIdentifier: "\(identifier)")
           UIApplication.shared.windows.first?.rootViewController = vc
           UIApplication.shared.windows.first?.makeKeyAndVisible()
       }
    }
}

var vActivityIndicator : NVActivityIndicatorView?
var vSpinner : UIView?

extension UIViewController {
    
    func showSpinner() {
        guard let parentView = UIApplication.shared.firstKeyWindow else{
            return
        }
        
        let spinnerView = UIView.init(frame: parentView.bounds)
        let visualBlur = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visualBlur.frame = spinnerView.bounds
        let frame = CGRect(x: spinnerView.center.x, y: spinnerView.center.y, width: 55.0, height: 50.0)
        let activityIndicator = NVActivityIndicatorView(frame: frame, type: .ballScaleRippleMultiple, color: .white)
        activityIndicator.center = spinnerView.center
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4) {
                spinnerView.addSubview(activityIndicator)
                spinnerView.insertSubview(visualBlur, belowSubview: activityIndicator)
                parentView.addSubview(spinnerView)
                activityIndicator.startAnimating()
            }
        }
        
        vActivityIndicator = activityIndicator
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                vSpinner?.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                vSpinner?.alpha = 0
                vActivityIndicator?.stopAnimating()

            }) { (true) in
                vSpinner?.removeFromSuperview()
            }
        }
    }
    
    func alertSingleBtn(title: String, message: String, btnTitle: String, completion: @escaping (Bool)->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okayBtn = UIAlertAction(title: btnTitle, style: .default) { (action:UIAlertAction) in
            completion(true)
        }
        alert.addAction(okayBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertDoubleBtn(title: String, message: String, firstBtnTitle: String, secondBtnTitle: String, completion: @escaping (Bool)->Void) {
           let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
           let okayBtn = UIAlertAction(title: firstBtnTitle, style: .default) { (action:UIAlertAction) in
               completion(true)
           }
           let noBtn = UIAlertAction(title: secondBtnTitle, style: .cancel)
           
           alert.addAction(noBtn)
           alert.addAction(okayBtn)
           self.present(alert, animated: true, completion: nil)
    }
}

extension UIApplication {

    /// The app's key window taking into consideration apps that support multiple scenes.
    var firstKeyWindow: UIWindow? {
        return windows.first(where: { $0.isKeyWindow })
    }

}
